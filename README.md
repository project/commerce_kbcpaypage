Commerce KBC Paypage
=====================

Provides a KBC Paypage payment gateway for Drupal Commerce.


## Requirements

* [Drupal commerce](https://www.drupal.org/project/commerce)


## Installation

First you have to download the module and its dependencies. The easiest way to do this is using Composer:

```
composer require drupal/commerce_kbcpaypage
```

Now enable the module as usuall and the KBC Paypage should become available.


## Configuration

### KBC Paypage

Browse to the KBC Paypage backoffice of the desired environment, either [Test](https://secure.paypage.be/Ncol/Test/Backoffice/)
or [Production](https://secure.paypage.be/Ncol/Prod/Backoffice/).

Navigate to _Configuration > Technical information_ and configure following settings:

* Global security parameters tab:
  * Hash algorithm: `SHA-512`
  * Character encoding: `UTF-8`

* Data and origin verification tab:
  * Merchant URL: your website URL
  * SHA-IN pass phrase: a random passphrase

* Transaction feedback tab:
  * HTTP redirection in the browser:
    * Uncheck `I would like to receive transaction feedback parameters on the redirection URLs`

  * Direct HTTP server-to-server request:
    * Timing of the request: `Online but switch to a deferred request when the online requests fail`
    * Post-payment URL (both fields): `https://yourwebsite.com/commerce-kbcpaypage/feedback`
    
  * Dynamic e-Commerce parameters:
    * Ensure `COMPLUS` is selected

  * Security for request parameters:
    * SHA-OUT pass phrase: a random passphrase

  * HTTP request for status changes:
    * Timing of the request: `For each offline status change`
    * URL: `https://yourwebsite.com/commerce-kbcpaypage/update`


### Drupal

The Drupal configuration is as easy as creating a new `KBC Paypage` payment gateway via
`/admin/commerce/config/payment-gateways`.

Please note that your credentials will be saved as configuration and thus will be exported by default.
You might want to exclude them via [Config Ignore](https://www.drupal.org/project/config_ignore)
or a similar module.
