<?php

namespace Drupal\commerce_kbcpaypage\Plugin\Commerce\PaymentMethodType;

use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\PaymentMethodTypeBase;

/**
 * Provides the KBC Paypage payment method type.
 *
 * @CommercePaymentMethodType(
 *   id = "kbcpaypage",
 *   label = @Translation("KBC Paypage"),
 * )
 */
class Kbcpaypage extends PaymentMethodTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildLabel(PaymentMethodInterface $payment_method) {
    return $this->getLabel();
  }

}
