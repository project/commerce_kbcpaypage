<?php

namespace Drupal\commerce_kbcpaypage\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the KBC Paypage payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "kbcpaypage",
 *   label = @Translation("KBC Paypage"),
 *   display_label = @Translation("KBC Paypage"),
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_kbcpaypage\PluginForm\KbcpaypageOffsiteForm",
 *   },
 *   payment_method_types = {"kbcpaypage"},
 *   requires_billing_information = TRUE,
 * )
 */
class Kbcpaypage extends OffsitePaymentGatewayBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $config = [
      'credentials' => [
        'test' => [
          'pspid' => '',
          'passphrase_in' => '',
          'passphrase_out' => '',
        ],
        'live' => [
          'pspid' => '',
          'passphrase_in' => '',
          'passphrase_out' => '',
        ],
      ],
      'language' => 'en_US',
      'title' => '',
      'template' => NULL,
      'logo' => 'drupal',
      'font' => 'Arial',
      'colors' => [
        'general' => [
          'text' => '#000000',
          'background' => '#ffffff',
        ],
        'table' => [
          'text' => '#000000',
          'background' => '#ffffff',
        ],
        'button' => [
          'text' => '#000000',
          'background' => '#ffffff',
        ],
      ],
    ];

    return $config + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['general'] = [
      '#type' => 'details',
      '#title' => $this->t('General'),
      '#open' => TRUE,
      '#parents' => $form['#parents'],
    ];

    $form['general']['language'] = [
      '#type' => 'select',
      '#title' => $this->t('Language'),
      '#options' => [
        'nl_NL' => $this->t('Dutch'),
        'en_US' => $this->t('English'),
        'fr_FR' => $this->t('French'),
        'de_DE' => $this->t('German'),
      ],
      '#default_value' => $this->configuration['language'],
      '#required' => TRUE,
    ];

    $form['general']['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#default_value' => $this->configuration['title'],
      '#required' => TRUE,
    ];

    $form['credentials'] = [
      '#type' => 'details',
      '#title' => $this->t('Credentials'),
      '#open' => TRUE,
    ];

    foreach ($this->getSupportedModes() as $mode => $label) {
      $form['credentials'][$mode] = [
        '#type' => 'details',
        '#title' => $label,
        '#open' => TRUE,
      ];

      $form['credentials'][$mode]['pspid'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Username'),
        '#default_value' => $this->configuration['credentials'][$mode]['pspid'],
        '#required' => TRUE,
      ];

      $form['credentials'][$mode]['passphrase_in'] = [
        '#type' => 'textfield',
        '#title' => $this->t('In passphrase'),
        '#default_value' => $this->configuration['credentials'][$mode]['passphrase_in'],
        '#required' => TRUE,
      ];

      $form['credentials'][$mode]['passphrase_out'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Out passphrase'),
        '#default_value' => $this->configuration['credentials'][$mode]['passphrase_out'],
        '#required' => TRUE,
      ];
    }

    $form['template'] = [
      '#type' => 'details',
      '#title' => $this->t('Template'),
      '#parents' => $form['#parents'],
    ];

    $source = 'none';
    if ($this->configuration['template'] === 'drupal') {
      $source = 'drupal';
    }
    elseif ($this->configuration['template'] !== NULL) {
      $source = 'kbc';
    }

    $source_name = $form['#parents'];
    $source_name[] = 'template_source';
    $source_name = array_shift($source_name) . '[' . implode('][', $source_name) . ']';

    $form['template']['template_source'] = [
      '#type' => 'radios',
      '#title' => $this->t('Source'),
      '#options' => [
        'none' => $this->t('None'),
        'kbc' => $this->t('File hosted at KBC Paypage'),
        'drupal' => $this->t('Drupal template'),
      ],
      '#default_value' => $source,
      '#required' => TRUE,
    ];

    $form['template']['template_file'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Filename'),
      '#default_value' => $source === 'kbc' ? $this->configuration['template'] : NULL,
      '#states' => [
        'visible' => [
          ':input[name="' . $source_name . '"]' => ['value' => 'kbc'],
        ],
        'required' => [
          ':input[name="' . $source_name . '"]' => ['value' => 'kbc'],
        ],
      ],
    ];

    $form['logo'] = [
      '#type' => 'details',
      '#title' => $this->t('Logo'),
      '#parents' => $form['#parents'],
    ];

    $source = 'none';
    if ($this->configuration['logo'] === 'drupal') {
      $source = 'drupal';
    }
    elseif ($this->configuration['logo'] !== NULL) {
      $source = 'kbc';
    }

    $source_name = $form['#parents'];
    $source_name[] = 'logo_source';
    $source_name = array_shift($source_name) . '[' . implode('][', $source_name) . ']';

    $form['logo']['logo_source'] = [
      '#type' => 'radios',
      '#title' => $this->t('Source'),
      '#options' => [
        'none' => $this->t('None'),
        'kbc' => $this->t('File hosted at KBC Paypage'),
        'drupal' => $this->t('Site logo'),
      ],
      '#default_value' => $source,
      '#required' => TRUE,
    ];

    $form['logo']['logo_file'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Filename'),
      '#default_value' => $source === 'kbc' ? $this->configuration['logo'] : NULL,
      '#states' => [
        'visible' => [
          ':input[name="' . $source_name . '"]' => ['value' => 'kbc'],
        ],
        'required' => [
          ':input[name="' . $source_name . '"]' => ['value' => 'kbc'],
        ],
      ],
    ];

    $form['css'] = [
      '#type' => 'details',
      '#title' => $this->t('Styling'),
      '#parents' => $form['#parents'],
    ];

    $form['css']['font'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Font Family'),
      '#default_value' => $this->configuration['font'],
      '#required' => TRUE,
    ];

    $form['css']['colors']['general']['text'] = [
      '#type' => 'color',
      '#title' => $this->t('Text color'),
      '#default_value' => $this->configuration['colors']['general']['text'],
      '#required' => TRUE,
    ];

    $form['css']['colors']['general']['background'] = [
      '#type' => 'color',
      '#title' => $this->t('Background color'),
      '#default_value' => $this->configuration['colors']['general']['background'],
      '#required' => TRUE,
    ];

    $form['css']['colors']['table'] = [
      '#type' => 'details',
      '#title' => $this->t('Table colors'),
      '#parents' => $form['#parents'],
    ];

    $form['css']['colors']['button'] = [
      '#type' => 'details',
      '#title' => $this->t('Button colors'),
      '#parents' => $form['#parents'],
    ];

    foreach (['table', 'button'] as $type) {
      $form['css']['colors'][$type]['text'] = [
        '#type' => 'color',
        '#title' => $this->t('Text'),
        '#default_value' => $this->configuration['colors'][$type]['text'],
        '#required' => TRUE,
      ];

      $form['css']['colors'][$type]['background'] = [
        '#type' => 'color',
        '#title' => $this->t('Background'),
        '#default_value' => $this->configuration['colors'][$type]['background'],
        '#required' => TRUE,
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);

    $values = $form_state->getValue($form['#parents']);

    if ($values['template_source'] === 'kbc') {
      $name = $form['#parents'];
      $name[] = 'template_file';
      $name = implode('][', $name);

      if ($values['template_file'] === '') {
        $form_state->setErrorByName($name, $this->t('The template filename is required.'));
      }
      elseif (!preg_match('#^[a-z0-9_\-]+\.html?$#', $values['template_file'])) {
        $form_state->setErrorByName($name, $this->t('The template filename is not valid.'));
      }
    }

    if ($values['logo_source'] === 'kbc') {
      $name = $form['#parents'];
      $name[] = 'logo_file';
      $name = implode('][', $name);

      if ($values['logo_file'] === '') {
        $form_state->setErrorByName($name, $this->t('The logo filename is required.'));
      }
      elseif (!preg_match('#^[a-z0-9_\-]+\.(png|jpe?g|gif)$#i', $values['logo_file'])) {
        $form_state->setErrorByName($name, $this->t('The logo filename is not valid.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $values = $form_state->getValue($form['#parents']);
    $this->configuration['credentials'] = $values['credentials'];
    $this->configuration['language'] = $values['language'];
    $this->configuration['title'] = $values['title'];
    $this->configuration['font'] = $values['font'];

    foreach (['template', 'logo'] as $type) {
      switch ($values[$type . '_source']) {
        case 'none':
          $this->configuration[$type] = NULL;
          break;

        case 'kbc':
          $this->configuration[$type] = $values[$type . '_file'];
          break;

        case 'drupal':
          $this->configuration[$type] = 'drupal';
          break;
      }
    }

    $this->configuration['colors'] = $values['colors'];
  }

}
