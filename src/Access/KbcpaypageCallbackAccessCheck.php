<?php

namespace Drupal\commerce_kbcpaypage\Access;

use Drupal\commerce_kbcpaypage\KbcPaypageParameters;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Access check for the KBC Paypage callbacks.
 */
class KbcpaypageCallbackAccessCheck implements AccessInterface {

  /**
   * The parameters that are included in the signature.
   */
  protected const SIGNATURE_REQUEST_PARAMETERS = [
    'aavaddress', 'aavcheck', 'aavmail', 'aavname', 'aavphone', 'aavzip', 'acceptance',
    'alias', 'amount', 'bic', 'bin', 'brand', 'cardno', 'cccty', 'cn', 'collector_bic',
    'collector_iban', 'completionid', 'complus', 'creation_status', 'creditdebit',
    'currency', 'cvccheck', 'dcc_commpercentage', 'dcc_convamount', 'dcc_convccy',
    'dcc_exchrate', 'dcc_exchratesource', 'dcc_exchratets', 'dcc_indicator',
    'dcc_marginpercentage', 'dcc_validhours', 'deviceid', 'digestcardno', 'eci', 'ed',
    'email', 'enccardno', 'fxamount', 'fxcurrency', 'ip', 'ipcty', 'mandateid',
    'mobilemode', 'nbremailusage', 'nbripusage', 'nbripusage_alltx', 'nbrusage',
    'ncerror', 'orderid', 'payid', 'payidsub', 'payment_reference', 'pm',
    'requestcompletionid', 'sco_category', 'scoring', 'sequencetype', 'signdate',
    'status', 'subbrand', 'subscription_id', 'ticket', 'trxdate', 'vc',
  ];

  /**
   * The payment storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $paymentStorage;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->paymentStorage = $entity_type_manager->getStorage('commerce_payment');
  }

  /**
   * Perform the access check.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access check result.
   */
  public function access(Request $request) {
    $parameters = KbcPaypageParameters::createFromRequest($request);

    // Ensure all required parameters are present.
    foreach (['orderid', 'payid', 'status', 'complus', 'shasign'] as $parameter) {
      if (!$parameters->has($parameter)) {
        return AccessResult::forbidden($parameter . ' is missing');
      }
    }

    // Get the payment.
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface|null $payment */
    $payment = $this->paymentStorage->load((int) $parameters->get('complus'));

    if (!$payment) {
      return AccessResult::forbidden('None-existing payment');
    }

    // Get the passphrase.
    $passphrase = $this->getSignaturePassphrase($payment);

    if ($passphrase === NULL) {
      return AccessResult::forbidden('Passphrase not configured');
    }

    // Calculate and check the signature.
    $signature = $parameters->get('shasign');
    $parameters->reduce(self::SIGNATURE_REQUEST_PARAMETERS);
    $signature_calculated = $parameters->calcSignature($passphrase);

    if ($signature === $signature_calculated) {
      return AccessResult::allowed();
    }

    return AccessResult::forbidden('Invalid signature');
  }

  /**
   * Get the signature passphrase.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment.
   *
   * @return string|null
   *   The passphrase or NULL if not configured.
   */
  protected function getSignaturePassphrase(PaymentInterface $payment) {
    $payment_gateway = $payment->getPaymentGateway();

    if (!$payment_gateway) {
      return NULL;
    }

    $payment_gateway_plugin = $payment_gateway->getPlugin();
    $mode = $payment_gateway_plugin->getMode();
    $configuration = $payment_gateway_plugin->getConfiguration();

    return $configuration['credentials'][$mode]['passphrase_out'] ?? NULL;
  }

}
