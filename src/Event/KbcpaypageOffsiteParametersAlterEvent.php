<?php

namespace Drupal\commerce_kbcpaypage\Event;

use Drupal\commerce_kbcpaypage\KbcPaypageParameters;
use Drupal\Component\EventDispatcher\Event;

/**
 * Defines the KBC Paypage parameters alter event.
 */
class KbcpaypageOffsiteParametersAlterEvent extends Event {

  /**
   * The KBC Paypage parameters.
   *
   * @var \Drupal\commerce_kbcpaypage\KbcPaypageParameters
   */
  protected KbcPaypageParameters $parameters;

  /**
   * Constructs a KBC Paypage parameters alter event object.
   *
   * @param \Drupal\commerce_kbcpaypage\KbcPaypageParameters $parameters
   *   The KBC Paypage parameters.
   */
  public function __construct(KbcPaypageParameters $parameters) {
    $this->parameters = $parameters;
  }

  /**
   * Gets the KBC Paypage parameters.
   *
   * @return \Drupal\commerce_kbcpaypage\KbcPaypageParameters
   *   The KBC Paypage parameters that have been set for this event instance.
   */
  public function getParameters(): KbcPaypageParameters {
    return $this->parameters;
  }

}
