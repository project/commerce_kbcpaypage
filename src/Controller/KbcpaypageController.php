<?php

namespace Drupal\commerce_kbcpaypage\Controller;

use Drupal\commerce_kbcpaypage\KbcPaypageParameters;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\PaymentGatewayInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\HtmlResponse;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller to handle requestd from KBC Paypage.
 */
class KbcpaypageController extends ControllerBase {

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(RendererInterface $renderer) {
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('renderer')
    );
  }

  /**
   * Process the payment feedback.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Drupal\Core\Render\HtmlResponse
   *   The response.
   */
  public function feedback(Request $request) {
    $parameters = KbcPaypageParameters::createFromRequest($request);

    // Log the feedback.
    $status = (int) $parameters->get('status');
    $logger = $this->getLogger('commerce_kbcpaypage');

    if ($parameters->has('ncerror') && $parameters->get('ncerror')) {
      $logger->info('Received payment feedback for order @order, the status is @status (@description): %error.', [
        '@order' => $parameters->get('orderid'),
        '@status' => $status,
        '@description' => $this->getPaymentStatusDescription($status),
        '%error' => $parameters->get('ncerror'),
      ]);
    }
    else {
      $logger->info('Received payment feedback for order @order, the status is @status (@description).', [
        '@order' => $parameters->get('orderid'),
        '@status' => $status,
        '@description' => $this->getPaymentStatusDescription($status),
      ]);
    }

    // Process it.
    $this->processPaymentCallback($parameters);

    return new HtmlResponse('');
  }

  /**
   * Process a payment update.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Drupal\Core\Render\HtmlResponse
   *   The response.
   */
  public function update(Request $request) {
    $parameters = KbcPaypageParameters::createFromRequest($request);

    // Log this update.
    $status = (int) $parameters->get('status');
    $logger = $this->getLogger('commerce_kbcpaypage');

    if ($parameters->has('ncerror')) {
      $logger->info('Received payment update for order @order, the new status is @status (@description): %error.', [
        '@order' => $parameters->get('orderid'),
        '@status' => $status,
        '@description' => $this->getPaymentStatusDescription($status),
        '%error' => $parameters->get('ncerror'),
      ]);
    }
    else {
      $logger->info('Received payment update for order @order, the new status is @status (@description).', [
        '@order' => $parameters->get('orderid'),
        '@status' => $status,
        '@description' => $this->getPaymentStatusDescription($status),
      ]);
    }

    // Process it.
    $this->processPaymentCallback($parameters);

    return new HtmlResponse('');
  }

  /**
   * Get the payment template.
   *
   * @param \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\PaymentGatewayInterface $commerce_payment_gateway
   *   The payment gateway.
   *
   * @return \Drupal\Core\Render\HtmlResponse
   *   The template as HTML response.
   */
  public function getTemplate(PaymentGatewayInterface $commerce_payment_gateway) {
    // Render the template.
    $context = new RenderContext();
    $renderer = $this->renderer;
    $markup = $renderer->executeInRenderContext($context, function () use ($commerce_payment_gateway, $renderer) {
      $elements = [
        '#theme' => 'kbcpaypage_template__' . $commerce_payment_gateway->getDerivativeId(),
        '#payment_gateway' => $commerce_payment_gateway,
      ];

      return $renderer->render($elements);
    });

    // Create the response and add the cache metadata.
    $response = HtmlResponse::create($markup);

    if (!$context->isEmpty()) {
      $response->addCacheableDependency($context->pop());
    }

    $max_age = $response->getCacheableMetadata()->getCacheMaxAge();

    if ($max_age === CacheBackendInterface::CACHE_PERMANENT) {
      $max_age = $this->config('system.performance')->get('cache.page.max_age');
    }

    $response->setCache([
      'public' => TRUE,
      'max_age' => $max_age,
    ]);

    return $response;
  }

  /**
   * Get the description for a payment status code.
   *
   * @param int $status
   *   The payment status.
   *
   * @return string
   *   The description.
   */
  protected function getPaymentStatusDescription($status) {
    switch ($status) {
      case 0:
        return 'Invalid or incomplete';

      case 1:
        return 'Cancelled by customer';

      case 2:
        return 'Authorisation refused';

      case 4:
        return 'Order stored';

      case 40:
        return 'Stored waiting external result';

      case 41:
        return 'Waiting for client payment';

      case 46:
        return 'Waiting authentication';

      case 5:
        return 'Authorised';

      case 50:
        return 'Authorized waiting external result';

      case 51:
        return 'Authorisation waiting';

      case 52:
        return 'Authorisation not known';

      case 55:
        return 'Standby';

      case 56:
        return 'Ok with scheduled payments';

      case 57:
        return 'Not OK with scheduled payments';

      case 59:
        return 'Authorization to be requested manually';

      case 6:
      case 64:
        return 'Authorised and cancelled';

      case 61:
        return 'Authorisation deletion waiting';

      case 62:
        return 'Authorisation deletion uncertain';

      case 63:
        return 'Authorisation deletion refused';

      case 7:
      case 74:
        return 'Payment deleted';

      case 71:
        return 'Payment deletion pending';

      case 72:
        return 'Payment deletion uncertain';

      case 73:
        return 'Payment deletion refused';

      case 8:
      case 84:
        return 'Refund';

      case 81:
        return 'Refund pending';

      case 82:
        return 'Refund uncertain';

      case 83:
        return 'Refund refused';

      case 85:
        return 'Refund handled by merchant';

      case 9:
        return 'Payment requested';

      case 91:
        return 'Payment processing';

      case 92:
        return 'Payment uncertain';

      case 93:
        return 'Payment refused';

      case 94:
        return 'Refund declined by the acquirer';

      case 95:
        return 'Payment handled by merchant';

      case 96:
        return 'Refund reversed';

      case 99:
        return 'Being processed';
    }

    return '';
  }

  /**
   * Process the payment callback.
   *
   * @param \Drupal\commerce_kbcpaypage\KbcPaypageParameters $parameters
   *   The KBC Paypage parameters.
   *
   * @return \Drupal\commerce_payment\Entity\PaymentInterface
   *   The payment.
   */
  protected function processPaymentCallback(KbcPaypageParameters $parameters) {
    $status = (int) $parameters->get('status');

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entityTypeManager()
      ->getStorage('commerce_payment')
      ->load((int) $parameters->get('complus'));

    $payment
      ->setRemoteId($parameters->get('payid'))
      ->setRemoteState($status);

    if (in_array($status, [5, 92, 93], TRUE)) {
      $payment->setState('authorized');
    }
    elseif (in_array($status, [9, 95], TRUE)) {
      $payment->setState('completed');
    }
    elseif (in_array($status, [6, 64, 7, 74], TRUE)) {
      $payment->setState('authorization_voided');
    }

    $payment->save();

    return $payment;
  }

}
