<?php

namespace Drupal\commerce_kbcpaypage;

use Symfony\Component\HttpFoundation\Request;

/**
 * The KBC Paypage parameters.
 */
class KbcPaypageParameters {

  /**
   * The parameters.
   *
   * @var array
   */
  protected $parameters = [];

  /**
   * Class constructor.
   *
   * @param array $parameters
   *   The initial parameters to add.
   */
  public function __construct(array $parameters = []) {
    $this->add($parameters);
  }

  /**
   * Create an instance for an incoming request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request from KBC Paypage.
   *
   * @return self
   *   A class instance.
   */
  public static function createFromRequest(Request $request) {
    $property = $request->isMethod('POST') ? 'request' : 'query';
    $parameters = $request->{$property}->all();
    $parameters = array_filter($parameters, static function ($value) {
      return is_string($value) && $value !== '';
    });

    return new self($parameters);
  }

  /**
   * Get all parameters.
   *
   * @return array
   *   An associative array of all parameters.
   */
  public function all() {
    return $this->parameters;
  }

  /**
   * Get a parameter.
   *
   * @param string $parameter
   *   The parameter name.
   *
   * @return string
   *   The parameter value or NULL if missing.
   */
  public function get($parameter) {
    $parameter = $this->prepareParameterName($parameter);
    return $this->parameters[$parameter] ?? NULL;
  }

  /**
   * Check if a parameter exists.
   *
   * @param string $parameter
   *   The parameter name.
   *
   * @return bool
   *   TRUE if the parameter exists.
   */
  public function has($parameter) {
    $parameter = $this->prepareParameterName($parameter);
    return isset($this->parameters[$parameter]);
  }

  /**
   * Set a parameter.
   *
   * @param string $parameter
   *   The parameter name.
   * @param string|int|null $value
   *   The parameter value, set to an empty string or equal to remove.
   *
   * @return $this
   */
  public function set($parameter, $value) {
    $value = (string) $value;

    if ($value === '') {
      return $this->remove($parameter);
    }

    $parameter = $this->prepareParameterName($parameter);
    $this->parameters[$parameter] = $value;

    return $this;
  }

  /**
   * Add a set of parameters.
   *
   * @param array $parameters
   *   The parameters to add.
   *
   * @return $this
   */
  public function add(array $parameters) {
    foreach ($parameters as $parameter => $value) {
      $this->set($parameter, $value);
    }

    return $this;
  }

  /**
   * Remove a parameter.
   *
   * @param string $parameter
   *   The parameter name.
   *
   * @return $this
   */
  public function remove($parameter) {
    $parameter = $this->prepareParameterName($parameter);
    unset($this->parameters[$parameter]);

    return $this;
  }

  /**
   * Reduce the parameters to the specified list.
   *
   * @param array $parameters
   *   The parameters to keep.
   *
   * @return $this
   */
  public function reduce(array $parameters) {
    $parameters = array_map([$this, 'prepareParameterName'], $parameters);

    $this->parameters = array_intersect_key(
      $this->parameters,
      array_flip($parameters)
    );

    return $this;
  }

  /**
   * Get the number of parameters.
   *
   * @return int
   *   The number of parameters.
   */
  public function count() {
    return count($this->parameters);
  }

  /**
   * Calculate the signature for all parameters.
   *
   * @param string $passphrase
   *   The passphrase.
   *
   * @return string
   *   The signature.
   */
  public function calcSignature($passphrase) {
    ksort($this->parameters);

    $signature = '';
    foreach ($this->parameters as $param => $value) {
      $signature .= $param . '=' . $value . $passphrase;
    }

    return mb_strtoupper(hash('sha512', $signature));
  }

  /**
   * Prepare a parameter name.
   *
   * @param string $parameter
   *   The parameter name.
   *
   * @return string
   *   The prepared parameter name.
   */
  protected function prepareParameterName($parameter) {
    return mb_strtoupper($parameter);
  }

}
