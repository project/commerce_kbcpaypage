<?php

namespace Drupal\commerce_kbcpaypage\PluginForm;

use Drupal\commerce_kbcpaypage\Event\KbcpaypageOffsiteParametersAlterEvent;
use Drupal\commerce_kbcpaypage\KbcPaypageParameters;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\PaymentGatewayInterface;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm;
use Drupal\commerce_price\MinorUnitsConverterInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Provides the offsite form for KBC Paypage.
 */
class KbcpaypageOffsiteForm extends PaymentOffsiteForm implements ContainerInjectionInterface {

  /**
   * The minor units converter.
   *
   * @var \Drupal\commerce_price\MinorUnitsConverterInterface
   */
  protected MinorUnitsConverterInterface $minorUnitsConverter;

  /**
   * An event dispatcher instance.
   *
   * @var \Symfony\Contracts\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Class constructor.
   *
   * @param \Drupal\commerce_price\MinorUnitsConverterInterface $minor_units_converter
   *   The minor units converter.
   * @param \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *    An event dispatcher instance.
   */
  public function __construct(
    MinorUnitsConverterInterface $minor_units_converter,
    EventDispatcherInterface $event_dispatcher
  ) {
    $this->minorUnitsConverter = $minor_units_converter;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('commerce_price.minor_units_converter'),
      $container->get('event_dispatcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    // Get and save the payment so it has an ID.
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    $payment->save();

    // Get the payment gateway plugin.
    $payment_gateway_plugin = $payment
      ->getPaymentGateway()
      ->getPlugin();

    // Get the parameters.
    $parameters = $this->getPluginParameters($payment_gateway_plugin)
      ->set('orderid', $payment->getOrderId())
      ->set('complus', $payment->id())
      ->set('accepturl', $form['#return_url'])
      ->set('declineurl', $form['#return_url'])
      ->set('exceptionurl', $form['#return_url'])
      ->set('cancelurl', $form['#cancel_url']);

    // Add the amount and currency.
    /** @var \Drupal\commerce_price\Price $amount */
    $amount = $payment->getAmount();
    $parameters
      ->set('amount', $this->minorUnitsConverter->toMinorUnits($amount))
      ->set('currency', $amount->getCurrencyCode());

    // Add the customer details.
    /** @var \Drupal\commerce_order\Entity\Order $order */
    $order = $payment->getOrder();

    /** @var \Drupal\profile\Entity\ProfileInterface $billing_profile */
    $billing_profile = $order->getBillingProfile();

    /** @var \Drupal\address\AddressInterface $billing_address */
    $billing_address = $billing_profile->get('address')->first();

    $parameters
      ->set('cn', $billing_address->getGivenName() . ' ' . $billing_address->getFamilyName())
      ->set('email', $order->getEmail())
      ->set('ownerzip', $billing_address->getPostalCode())
      ->set('owneraddress', $billing_address->getAddressLine1())
      ->set('ownercty', $billing_address->getCountryCode())
      ->set('ownertown', $billing_address->getLocality());

    // Allow other modules to customise the parameters.
    $this->eventDispatcher->dispatch(
      new KbcpaypageOffsiteParametersAlterEvent($parameters)
    );

    // Calculate and add the signature.
    $mode = $payment_gateway_plugin->getMode();
    $passphrase = $payment_gateway_plugin->getConfiguration()['credentials'][$mode]['passphrase_in'];
    $parameters->set('shasign', $parameters->calcSignature($passphrase));

    // Return a redirect form.
    return $this->buildRedirectForm(
      $form,
      $form_state,
      $this->getRedirectUrl($mode),
      $parameters->all(),
      self::REDIRECT_POST
    );
  }

  /**
   * Get the initial KBC Paypage parameters from the payment gateway plugin.
   *
   * @param \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\PaymentGatewayInterface $payment_gateway_plugin
   *   The payment gateway plugin instance.
   *
   * @return \Drupal\commerce_kbcpaypage\KbcPaypageParameters
   *   The KBC Paypage parameters.
   */
  protected function getPluginParameters(PaymentGatewayInterface $payment_gateway_plugin) {
    $configuration = $payment_gateway_plugin->getConfiguration();
    $colors = $configuration['colors'];

    $parameters = (new KbcPaypageParameters())
      ->set('pspid', $configuration['credentials'][$payment_gateway_plugin->getMode()]['pspid'])
      ->set('language', $configuration['language'])
      ->set('title', $configuration['title'])
      ->set('fonttype', $configuration['font'])
      ->set('txtcolor', $colors['general']['text'])
      ->set('bgcolor', $colors['general']['background'])
      ->set('tbltxtcolor', $colors['table']['text'])
      ->set('tblbgcolor', $colors['table']['background'])
      ->set('buttontxtcolor', $colors['button']['text'])
      ->set('buttonbgcolor', $colors['button']['background']);

    // Template.
    if ($configuration['template']) {
      $template = $configuration['template'];

      if ($template === 'drupal') {
        $template = Url::fromRoute('commerce_kbcpaypage.template', [
          'commerce_payment_gateway' => $payment_gateway_plugin->getDerivativeId(),
        ])->setAbsolute()->toString();
      }

      $parameters->set('tp', $template);
    }

    // Logo.
    if ($configuration['logo']) {
      $logo = $configuration['logo'];

      if ($logo === 'drupal') {
        $logo = theme_get_setting('logo.url');

        if ($logo) {
          $logo = Url::fromUserInput($logo)
            ->setAbsolute()
            ->toString();
        }
      }

      $parameters->set('logo', $logo);
    }

    return $parameters;
  }

  /**
   * Get the redirect URL.
   *
   * @param string $mode
   *   The mode in which the payment gateway is operating.
   *
   * @return string
   *   The redirect URL.
   */
  protected function getRedirectUrl($mode) {
    if ($mode === 'test') {
      $default = 'https://secure.paypage.be/ncol/test/orderstandard_utf8.asp';
    }
    else {
      $default = 'https://secure.paypage.be/ncol/prod/orderstandard_utf8.asp';
    }

    return Settings::get('commerce_kbcpaypage_url_' . $mode, $default);
  }

}
