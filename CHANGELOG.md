Changelog
=========

All notable changes to the Commerce KBC Paypage module.


## 1.1.0 (2024-05-04):

### Added

* Drupal 10 and 11 compatibility (#3444633)


## 1.0.1 (2022-05-29):

### Fixed

* Invalid usage of `->has()` in various places (#3257383)


## 1.0.0 (2021-09-06):

### Added

* License file and fixed indentation composer.json.


## 1.0.0-alpha1 (2021-03-22):

First alpha release.
